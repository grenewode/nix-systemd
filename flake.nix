{
  description = "A custom build of systemd for the latest features";

  inputs = {
    nixpkgs.url =
      "github:NixOS/nixpkgs/eeff6c493373d3fff11421b55309fab6a1d4ec7d";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, flake-utils, nixpkgs, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in rec {
        packages.systemd = pkgs.systemd;
        packages.default = pkgs.systemd;
        defaultPackage = pkgs.systemd;
      });
}
